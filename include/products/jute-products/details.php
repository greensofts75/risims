<h3>Jute Products</h3>

<div class="imagestack">
  <ul class="imagestack">    
    <?php
    $images = array(
        "include/products/jute-products/images/IMG_9022.jpg",
        "include/products/jute-products/images/IMG_9064.jpg",
        "include/products/jute-products/images/IMG_9065.jpg",
        "include/products/jute-products/images/IMG_9067.jpg",
        "include/products/jute-products/images/IMG_9073.jpg",
    );
    foreach ($images as $img) {
      echo "<li><a target='_blank' href='$img'  ><img src='$img' style='height:150px;'/></a></li>";
    }
    ?>
  </ul>  
</div>
<hr class="space">

<p>We manufacture  and supply Jute Hessian Cloth, hessian Bag, Jute Tape ,Jute Tape, Jute Webbing ,Jute Twine, Sacking Bag and many other of various specifications to suit user’s requirements. 
</p>


<table class="table table-condensed">                          
  <tr>
    <td> 
      <ul class="imagestack">
        <li><a target='_blank' href='include/products/jute-products/images/jute-product-1.jpg'><img src='include/products/jute-products/images/jute-product-1.jpg' height="150"/></a></li>
      </ul> </td>
    <td>
      <h4>Hessian Cloth</h4>
      <p>In our Jute Mills Jute Hessian Cloth can be manufactured in various sizes and widths from 22 inches width to 80 inches width. Jute Hessian Cloth can be supplied in natural colour or in any colour of your choice, packed either in bales or in rolls.</p>                  
      <p>SIZE:  Any sizes and weights made as per buyer’s requirement. Such as:  40”-7oz/40” ,40”-10oz/40” ,45”/7-oz/40”, 36”/7-oz/40”,36”/10oz/40”, 50”/10-oz/45”, 54”/11-oz/45” 11x12,  and many other </p>
    </td>
  </tr>
  <tr>
    <td> 
      <ul class="imagestack">
        <li><a target='_blank' href='include/products/jute-products/images/jute-product-2.jpg'><img src='include/products/jute-products/images/jute-product-2.jpg' height="150"/></a></li>
      </ul> </td>
    <td>
      <h4>Hessian Bag :</h4>
      <p>We offer to our clients superior quality Hessian Onion Bag. Due to high quality and durability these Hessian Onion Bag are famous among its user  and its used for packing various commodities such as grains, potatoes, onions, sugar, tobacco, coffee, cocoa, peanuts and sand. These bags are made so as to carry a weight  up to  60kgs. Further, these bags biodegradable and do not cause any harm to the environment.</p>
    </td>
  </tr>
  <tr>
    <td> 
      <ul class="imagestack">
        <li><a target='_blank' href='include/products/jute-products/images/jute-product-3.jpg'><img src='include/products/jute-products/images/jute-product-3.jpg' height="150"/></a></li>
      </ul> </td>
    <td>
      <h4>Sand Bag</h4>
      <p>We offer the wide variety of Jute Sand Bags that are manufactured from the premium quality of cloths. These bags come are extensively used on the sides of rivers to protect river banks. The entire range of our Jute Sand Bag is recognized for durability and minimum maintenance and performance. Offered by us at affordable prices, these bags are ideal for various industrial purposes</p>            
      <p>
      <ul>
        <li>Size: a) 35” x 15” Selvedge, Ex -35 -10/40, 11 x 12 P & S, plain, HK/OHDS, Single/two tie strings of 14” each at mouth, packing 1000 bags flat per bale.</li>
        <li>b) 34” x 14” Selvedge, Ex -34 -10/40, 11 x 12 P & S, plain, HK/OHDS, Single/two tie strings of 14” each at mouth, packing 1000 bags flat per bale.</li>
        <li> c) 33” x 14” Selvedge, Ex -33 -10/45, 11 x 12 P & S, plain, HK/OHDS, Single/two tie strings of 14” each at mouth, packing 1000 bags flat per bale.</li>
        <li>d) 28” x 14” Selvedge, Ex -28 -10/40, 11 x 12 P & S, plain, HK/OHDS, Single/two tie strings of 14” each at mouth, packing 1000 bags flat per bale</li>
      </ul>

      </p>
    </td>
  </tr>  
  <tr>
    <td> 
      <ul class="imagestack">
        <li><a target='_blank' href='include/products/jute-products/images/jute-product-4.jpg'><img src='include/products/jute-products/images/jute-product-4.jpg' height="150"/></a></li>
      </ul> </td>
    <td>
      <h4>Jute Tape</h4>
      <p>Jute tape used in the plantation mostly it is used for wrapping roots of the Three. Jute tape produced in different color. Colored Jute tape also used in decorative purpose.
        Size 10 cm up to 150 cm Ex-cloth 7 oz & 10 oz
      </p>            
      <p></p>
    </td>
  </tr>  
  <tr>
    <td> 
      <ul class="imagestack">
        <li><a target='_blank' href='include/products/jute-products/images/jute-product-5.jpg'><img src='include/products/jute-products/images/jute-product-5.jpg' height="150"/></a></li>
      </ul> </td>
    <td>
      <h4>Jute Sacking Bag</h4>
      <p>Jute Sacks are ideally used for packing all kinds of agriculture products (Rice, Wheat, Coffee, Beans, Pules, Corn, Shelled Nuts, Copra, Sugar, Tea, various spices etc.), Minerals & Fertilizer. The Jute sacking Bags products under treatment of Vegetable oil known as Hydro Carbon free Food Grade Bags, thus these Bags have a huge demand.</p>            
      <p></p>
    </td>
  </tr>  
  <tr>
    <td> 
      <ul class="imagestack">
        <li><a target='_blank' href='include/products/jute-products/images/jute-product-6.jpg'><img src='include/products/jute-products/images/jute-product-6.jpg' height="150"/></a></li>
      </ul> </td>
    <td>
      <h4>Jute Yarn / Twine</h4>
      <p>Catering to the increasing demands of the industry, we offer to our customers with high end quality of Jute Yarn. These yarns are made of high quality jute fibers and are widely used in the manufacturing of jute bags and other products in various industries across the country.
      </p>            
      <p></p>
    </td>
  </tr>  
  <tr>
    <td> 
      <ul class="imagestack">
        <li><a target='_blank' href='include/products/jute-products/images/jute-product-8.jpg'><img src='include/products/jute-products/images/jute-product-8.jpg' height="150"/></a></li>
      </ul> </td>
    <td>
      <h4>Jute Rope</h4>
      <p>Our company offers a wide assortment of Jute Ropes that are available in various enticing designs and eye catching patterns. Ideal for decoration and other purpose, these are available in different shapes, sizes and colors. Moreover, these ropes are easy to handle and thus hold knots securely. These Packed Jute Ropes can be customized as per the requirement of the clients.Size; 2 mm up to 200 mm ( Manual & machine made both available)</p>            
      <p></p>
    </td>
  </tr>  
  <tr>
    <td> 
      <ul class="imagestack">
        <li><a target='_blank' href='include/products/jute-products/images/jute-product-9.jpg'><img src='include/products/jute-products/images/jute-product-9.jpg' height="150"/></a></li>
      </ul> </td>
    <td>
      <h4>Jute Webbing</h4>
      <p>Jute Webbing has popularity in different industry. It is enormously used in cable industry and Carpet Industry. It is also used for making seat belt for car industry. Jute weaving is a tape made of jute which is strong and eco friendly. Width: 1.5"-3.5", Weight: 6 to 24 lbs per gross yard ( 144 yards) depending on width ; Design : plain and stripe; Packing : 6 and 12 gross yard per bale.</p>            
      <p></p>
    </td>
  </tr>  
</table>

<!-- /tabs -->