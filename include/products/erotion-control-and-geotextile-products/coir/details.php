<!-- tabs -->
<div class="tabbable">
  <ul class="nav nav-tabs">
    <li class="active"><a href="#coir-mesh" data-toggle="tab"><h3 style="margin-bottom: 5px">Coir Mesh</h3></a></li>
    <li><a href="#coir-log" data-toggle="tab"><h3 style="margin-bottom: 5px">Coir log</h3></a></li>    
  </ul>
  <div class="tab-content">
    <div class="tab-pane active" id="coir-mesh">
      <hr class="space">
      <p>
        Coir (Coconut fiber), is made from well cleaned, high strength, fresh water cured coconut fiber  . Coconut fiber  is extracted from coconut husks which are submerged in fresh water streams or storage tanks for a period of several months. During this process known as retting, water constantly flows through the coir fiber  matrix removing impurities present in the fiber. The clean fiber is extracted and spun into yarn which is then woven into Coir Mat. 
      </p>

      <div class="imagestack">
        <ul class="imagestack">    
          <?php
          $images = array(
              "include/products/erotion-control-and-geotextile-products/coir/coir-mesh/images/CCM 400 gsm.JPG",
              "include/products/erotion-control-and-geotextile-products/coir/coir-mesh/images/CCM 700 gsm.jpg",
              "include/products/erotion-control-and-geotextile-products/coir/coir-mesh/images/CCM 740 gsm.jpg",
              "include/products/erotion-control-and-geotextile-products/coir/coir-mesh/images/CCM 900 gsm.jpg",
          );
          foreach ($images as $img) {
            echo "<li><a target='_blank' href='$img'><img src='$img'/></a></li>";
          }
          ?>
        </ul>  
      </div>
      <hr class="space">
      <p>Coir Mat erosion control matting is highly versatile with a wide array of applications in bioengineering and erosion control. Coir Mat by far exceeds the demand for an environmentally friendly and economic erosion control product.</p>


      <table class="table table-condensed">
        <tr>
          <td> 
            <ul class="imagestack">
              <li><a target='_blank' href='include/products/erotion-control-and-geotextile-products/coir/coir-mesh/images/CCM 400 gsm-2.png'><img src='include/products/erotion-control-and-geotextile-products/coir/coir-mesh/images/CCM 400 gsm-2.png'/></a></li>
            </ul> </td>
          <td>
            <h4>COIRMESH: 400GSM</h4>
            <p>100% biodegradable, long-lasting, lightweight spun coir fabric.</p>

            <b>Features & Benefits</b>
            <ul>
              <li>100% biodegradable lightweight spun coir fabric</li>
              <li>Open-weave construction allows for hydro-seeding after installation </li>
              <li>Long-lasting (4-5 years), where short growing seasons are not conducive to rapid establishment of vegetation </li>
              <li>Handles water flows of up to 8 feet per second </li>
              <li>11.8 oz per square yard / 400 grams per square meter</li> 
            </ul>
          </td>
        </tr>
        <tr>
          <td> 
            <ul class="imagestack">
              <li><a target='_blank' href='include/products/erotion-control-and-geotextile-products/coir/coir-mesh/images/CCM 700 gsm-2.png'><img src='include/products/erotion-control-and-geotextile-products/coir/coir-mesh/images/CCM 700 gsm-2.png'/></a></li>
            </ul> </td>
          <td>
            <h4>COIRMESH: 700GSM</h4>
            <p>Applications have been built around the installation of natural gas pipelines. When pipes are buried along or beneath waterways, the fabric is used to aid re-vegetation along stream beds and on river embankments.</p>

            <b>Features & Benefits</b>
            <ul>
              <li>100% biodegradable lightweight spun coir fabric</li>
              <li>Open-weave construction allows for hydro-seeding after installation</li>
              <li>For use on extreme slopes</li>
              <li>Long-lasting (4-5 years), where short growing seasons are not conducive to rapid establishment of vegetation </li>
              <li>Handles water flows of up to 10 feet per second </li>
              <li>20.6 oz per square yard / 700 grams per square meter </li> 
              <li>50% open area </li> 
            </ul>
          </td>
        </tr>
        <tr>
          <td> 
            <ul class="imagestack">
              <li><a target='_blank' href='include/products/erotion-control-and-geotextile-products/coir/coir-mesh/images/CCM 900 gsm-2.png'><img src='include/products/erotion-control-and-geotextile-products/coir/coir-mesh/images/CCM 900 gsm-2.png'/></a></li>
            </ul> </td>
          <td>
            <h4>COIRMESH: 900GSM</h4>
            <p>100% biodegradable, long-lasting, durable, heavyweight spun coir fabric.</p>

            <b>Features & Benefits</b>
            <ul>
              <li>100% biodegradable lightweight spun coir fabric</li>
              <li>Open-weave construction allows for hydro-seeding after installation</li>
              <li>For use on extreme slopes(> 1:1) </li>
              <li>Long-lasting (4-5 years), where short growing seasons are not conducive to rapid establishment of vegetation </li>
              <li>Economical substitute for synthetics in silty soils and tidal areas </li>
              <li>Handles water flows of up to 15 feet per second </li>
              <li>26.6 or per square yard / 900 grams per square meter</li> 
              <li>39% open area </li> 
            </ul>
          </td>
        </tr>
      </table>

    </div>
    <div class="tab-pane" id="coir-log">
      <div class="tab-pane active" id="coir-mesh">
<!--        <hr class="space">
        <p>
          100% biodegradable, flexible, cost-effective sediment control system. 
          Coir Logs are strong and flexible and designed to aid in stabilization and re-vegetation of sites. Coir logs are often used on steep slopes or areas with exposure to waves or currents which cause instability on a site. 

        </p>

        <div class="imagestack">
          <ul class="imagestack">    
            <?php
            $images = array(
                "include/products/erotion-control-and-geotextile-products/coir/coir-log/images/Coir log (20).jpg",
                "include/products/erotion-control-and-geotextile-products/coir/coir-log/images/unnamed3.jpg",
            );
            foreach ($images as $img) {
              echo "<li><a target='_blank' href='$img'><img src='$img'/></a></li>";
            }
            ?>
          </ul>  
        </div>-->
        <hr class="space">    


        <table class="table table-condensed">
          <tr>
            <td> 
              <ul class="imagestack">
                <li><a target='_blank' href='include/products/erotion-control-and-geotextile-products/coir/coir-log/images/Coir log (20).jpg'><img src='include/products/erotion-control-and-geotextile-products/coir/coir-log/images/Coir log (20).jpg'/></a></li>
              </ul> </td>
            <td>
              <h4>Coir Logs</h4>
              <p>100% biodegradable, flexible, cost-effective sediment control system. <br/>
Coir Logs are strong and flexible and designed to aid in stabilization and re-vegetation of sites. Coir logs are often used on steep slopes or areas with exposure to waves or currents which cause instability on a site. 
</p>

              <b>Features & Benefits</b>
              <ul>
                <li>Maximum erosion prevention</li>
                <li>Stable Substrate</li>
                <li>Promote root growth</li>
                <li>Self Maintaining</li>
                <li>Durable alternate to hay bales/logs.</li> 
              </ul>
              <b>Longevity</b><br/>
              <p>Depending on its application and exposure, Coir Mat lasts two to five years. This extended longevity period allows enough time to establish adequate vegetation allowing erosion control the way nature intended.</p>
            </td>
          </tr>       
        </table>

      </div>
    </div>    
  </div>
</div>
<!-- /tabs -->