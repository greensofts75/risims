<h3>Jute mat</h3>
<div class="imagestack">
  <ul class="imagestack">    
    <?php
    $images = array(
        "include/products/erotion-control-and-geotextile-products/jute_mat/images/erosion-1.jpg",
        "include/products/erotion-control-and-geotextile-products/jute_mat/images/erosion-4.jpg",
        "include/products/erotion-control-and-geotextile-products/jute_mat/images/images.jpg",
        "include/products/erotion-control-and-geotextile-products/jute_mat/images/Janata_001.jpg",
        "include/products/erotion-control-and-geotextile-products/jute_mat/images/Janata_007.jpg",
        "include/products/erotion-control-and-geotextile-products/jute_mat/images/Janata_011.jpg",
        "include/products/erotion-control-and-geotextile-products/jute_mat/images/Janata_023.jpg",
        "include/products/erotion-control-and-geotextile-products/jute_mat/images/Janata_005.jpg"
    );
    foreach ($images as $img) {
      echo "<li><a target='_blank' href='$img'  ><img src='$img' style='height:150px;'/></a></li>";
    }
    ?>
  </ul>  
</div>
<hr class="space">
<p>Coir Mat erosion control matting is highly versatile with a wide array of applications in bioengineering and erosion control. Coir Mat by far exceeds the demand for an environmentally friendly and economic erosion control product.</p>








<hr class="space">    


<table class="table table-condensed">
  <tr>
    <td> 
      <ul class="imagestack">
        <li><a target='_blank' href='include/products/erotion-control-and-geotextile-products/jute_mat/images/Janata_005.jpg'><img src='include/products/erotion-control-and-geotextile-products/jute_mat/images/Janata_005.jpg' height="150"/></a></li>
      </ul> </td>
    <td>
      <h4>JUTE MAT 280gsm</h4>
      <p>Jute matting 280gsm (light) is a 100% biodegradable erosion control product designed to naturally degrade. It contains no bleach or synthetic materials. This light weight product can be installed over seeded slopes to hold the soil in place until vegetation takes hold. This is a short-term matting (12-24 months). Jute matting is a simple product which is quickly installed. You can use U shaped ground staples or stakes.
      </p>

      <b>Features & Benefits</b>
      <ul>
        <li>Natural, 100% bio-degradable.</li>
        <li>Retains moisture, promoting growth of seeded ground or planted vegetation.</li>              
      </ul>
    </td>
  </tr>  
  <tr>
    <td> 
      <ul class="imagestack">
        <li><a target='_blank' href='include/products/erotion-control-and-geotextile-products/jute_mat/images/Janata_007.jpg'><img src='include/products/erotion-control-and-geotextile-products/jute_mat/images/Janata_007.jpg' height="150"/></a></li>
      </ul> </td>
    <td>
      <h4>JUTE MAT 440gsm</h4>
      <p>Jute Matting 450gsm. Roll size 1.83m x 25m. 100% organic and will break down over time, commonly used to protect soils in areas exposed to wind or heavy rainfall. Has a life expectancy varying from 6 - 12 months depending on climatic conditions. Used for the stabilisation of embankments or batters, and inhibit weed growth during the establishment of revegetation.
      </p>            
    </td>
  </tr>  
  <tr>
    <td> 
      <ul class="imagestack">
        <li><a target='_blank' href='include/products/erotion-control-and-geotextile-products/jute_mat/images/Janata_011.jpg'><img src='include/products/erotion-control-and-geotextile-products/jute_mat/images/Janata_011.jpg' height="150"/></a></li>
      </ul> </td>
    <td>
      <h4>Jute Mat 750gsm Slit</h4>
      <p>Slitted Jute Matting comes in 750gsm with 4 or 6 slits every m2. Roll size 1.83m x 25m. 100% organic and will break down over time, commonly used to protect soils in areas exposed to wind or heavy rainfall. Has a life expectancy varying from 12 - 24 months depending on climatic conditions. Used for the stabilisation of embankments or batters, and inhibit weed growth during the establishment of revegetation. Pre-slitted with planting holes to suit a range of landscaping needs.
      </p>
    </td>
  </tr>  
  <tr>
    <td> 
      <ul class="imagestack">
        <li><a target='_blank' href='include/products/erotion-control-and-geotextile-products/jute_mat/images/Janata_023.jpg'><img src='include/products/erotion-control-and-geotextile-products/jute_mat/images/Janata_023.jpg' height="150"/></a></li>
      </ul> </td>
    <td>
      <h4>Jute Mat 750gsm</h4>
      <p>Jute Matting 750gsm. Roll size 1.83m x 25m. 100% organic and will break down over time, commonly used to protect soils in areas exposed to wind or heavy rainfall. Has a life expectancy varying from 12 - 24 months depending on climatic conditions. Used for the stabilization of embankments or batters, and inhibit weed growth during the establishment of vegetation.
      </p>            
    </td>
  </tr>  
</table>