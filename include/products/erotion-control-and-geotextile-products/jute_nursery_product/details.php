<h3>Jute nursery products</h3>

<div class="imagestack">
  <ul class="imagestack">    
    <?php
    $images = array(
        "include/products/erotion-control-and-geotextile-products/jute_nursery_product/images/image-1.jpg",
        "include/products/erotion-control-and-geotextile-products/jute_nursery_product/images/image-2.jpg",
    );
    foreach ($images as $img) {
      echo "<li><a target='_blank' href='$img'  ><img src='$img' style='height:150px;'/></a></li>";
    }
    ?>
  </ul>  
</div>
<hr class="space">

<p>Coir Mat erosion control matting is highly versatile with a wide array of applications in bioengineering and erosion control. Coir Mat by far exceeds the demand for an environmentally friendly and economic erosion control product.</p>


<table class="table table-condensed">                          
  <tr>
    <td> 
      <ul class="imagestack">
        <li><a target='_blank' href='include/products/erotion-control-and-geotextile-products/jute_nursery_product/images/image-1.jpg'><img src='include/products/erotion-control-and-geotextile-products/jute_nursery_product/images/image-1.jpg' height="150"/></a></li>
      </ul> </td>
    <td>
      <h4>Jute Nursery Products</h4>
      <p>Jute Nursery Pot: Our Jute Nursery Pots are only manufactured using the best quality jute fibers. The jute fibers are quality tested before they are used to manufacture the Nursery Pots. We provide them in different sizes as per the demands of the buyer. These Jute Nursery Pots are used to grow and protect the seedling. However, you can also hand these pots in your home to make look more attractive and close to nature.
      </p>            
      <p></p>
    </td>
  </tr>
  <tr>
    <td> 
      <ul class="imagestack">
        <li><a target='_blank' href='include/products/erotion-control-and-geotextile-products/jute_nursery_product/images/image-2.jpg'><img src='include/products/erotion-control-and-geotextile-products/jute_nursery_product/images/image-2.jpg' height="150"/></a></li>
      </ul> </td>
    <td>
      <h4>Jute Nursery sheets</h4>
      <p>
      <ul>
        <li>Applications : Used in the tobacco industry for export packing of Leaf Tobacco bales </li>
        <li>Nurseries, Horticulture and in the forestry industry for wrapping of tree roots during transport and transplantation, </li>
        <li>Rotting retardant sheets,</li>
      </ul>
      </p>            
      <p></p>
    </td>
  </tr>  
</table>

<!-- /tabs -->