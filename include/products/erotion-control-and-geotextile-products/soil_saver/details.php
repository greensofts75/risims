<h3>Soil saver</h3>

<div class="imagestack">
  <ul class="imagestack">    
    <?php
    $images = array(
        "include/products/erotion-control-and-geotextile-products/soil_saver/images/erosion-6.jpg",
        "include/products/erotion-control-and-geotextile-products/soil_saver/images/IMG_9023.jpg",
        "include/products/erotion-control-and-geotextile-products/soil_saver/images/IMG_9025.jpg",
        "include/products/erotion-control-and-geotextile-products/soil_saver/images/IMG_9028.jpg",
        "include/products/erotion-control-and-geotextile-products/soil_saver/images/IMG_9029.jpg",
        "include/products/erotion-control-and-geotextile-products/soil_saver/images/IMG_9030.jpg",
        "include/products/erotion-control-and-geotextile-products/soil_saver/images/IMG_9034.jpg",
        "include/products/erotion-control-and-geotextile-products/soil_saver/images/IMG_9075.jpg", 
        
    );
    foreach ($images as $img) {
      echo "<li><a target='_blank' href='$img'  ><img src='$img' style='height:150px;'/></a></li>";
    }
    ?>
  </ul>  
</div>
<hr class="space">

<p>These are ideal for purposes like soil erosion control, vegetation consolidation, agro-mulching, reinforcement, and protection of riverbanks & embankments, land reclamation. These are extensively used for preventing top soil erosion. However, these Jute Soil Savers find usage in highway, railway, housing, airport projects. Jute mesh helps retain moisture and allows water and light infiltration to encourage vegetation growth.<br/><br/>
Jute Mesh or Jute Soil Saver Useful on embankments and batters that have an existing vegetation cover. Suitable for short term erosion protection. We are producing different weight and type of Soil Saver.
</p>


<table class="table table-condensed">                          
  <tr>
    <td> 
      <ul class="imagestack">
        <li><a target='_blank' href='include/products/erotion-control-and-geotextile-products/soil_saver/images/IMG_9023.jpg'><img src='include/products/erotion-control-and-geotextile-products/soil_saver/images/IMG_9023.jpg' height="150"/></a></li>
      </ul> </td>
    <td>
      <h4>Soil Saver 400GSM</h4>
      <p>Jute Mesh Soil Saver 400GSM width 48”, weight 400grams/m2, 6.5x4.5/10cms, packed in bales  75 yards=600 yards /Bale
      </p>            
      <p></p>
    </td>
  </tr>
  <tr>
    <td> 
      <ul class="imagestack">
        <li><a target='_blank' href='include/products/erotion-control-and-geotextile-products/soil_saver/images/IMG_9025.jpg'><img src='include/products/erotion-control-and-geotextile-products/soil_saver/images/IMG_9025.jpg' height="150"/></a></li>
      </ul> </td>
    <td>
      <h4>Soil Saver 500GSM</h4>
      <p>
      Jute Mesh Soil Saver 500GSM ,loading quantity  52 bales = 31,200 yards in a 20’ft container, Soil Saver Cloth, width 122cms,weight 500grams/m2, 6.5x4.5/10cms,  packed 8 cuts of 75 yards = 600yards/bale 
      </p>            
      <p></p>
    </td>
  </tr>  
  <tr>
    <td> 
      <ul class="imagestack">
        <li><a target='_blank' href='include/products/erotion-control-and-geotextile-products/soil_saver/images/IMG_9028.jpg'><img src='include/products/erotion-control-and-geotextile-products/soil_saver/images/IMG_9028.jpg' height="150"/></a></li>
      </ul> </td>
    <td>
      <h4>Soil Saver 500GSM(Jumbo Bales</h4>
      <p>
      Jute Mesh Soil Saver 500GSM (Jumbo bakes) loading quantity  34 bales = 31,200 yards in a 20’ft container, Soil Saver Cloth, width 122cms, Size 48” ,weight 500grams/m2, 6.5x4.5/10cms,  packed 2 cuts of 450 yards = 900yards/bale 
      </p>            
      <p></p>
    </td>
  </tr>  
  <tr>
    <td> 
      <ul class="imagestack">
        <li><a target='_blank' href='include/products/erotion-control-and-geotextile-products/soil_saver/images/IMG_9029.jpg'><img src='include/products/erotion-control-and-geotextile-products/soil_saver/images/IMG_9029.jpg' height="150"/></a></li>
      </ul> </td>
    <td>
      <h4>Soil Saver 500GSM (Roll Form)</h4>
      <p>
      Jute Mesh Soil Saver 500GSm in Roll form usually shipped in a 40’ft container. In a 40’ft container loading quantity 567 rolls = 42,525yards, Soil Saver width 48”, weight 500grams/m2, 6.5x4.5/10cms, packed in rolls without paper tube & wrapping, 75 yards/roll 
      </p>            
      <p></p>
    </td>
  </tr>  
</table>

<!-- /tabs -->