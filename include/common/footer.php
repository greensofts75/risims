<div class='span-24 footer'>
  <span class='copyrights'>Copyright Information Goes Here 2012. All Rights Reserved. Designed by <a href='http://www.activationltd.com/' target='_blank' title='Activation'>Activation</a></span>
  <span class='socialFooter'>
    <a href='#' class='tooltip' data-tip='Twitter'><img src='images/social/twitter.png' alt='Twitter'></a>
    <a href='#' class='tooltip' data-tip='Tumblr'><img src='images/social/tumblr.png' alt='Tubmlr'></a>
    <a href='#' class='tooltip' data-tip='Facebook'><img src='images/social/facebook.png' alt='Facebook'></a>
    <a href='#' class='tooltip' data-tip='Vimeo'><img src='images/social/vimeo.png' alt='Vimeo'></a>
    <a href='#' class='tooltip' data-tip='Google+'><img src='images/social/google_plus_alt.png' alt='Google+'></a>
    <script type="text/javascript">
      $('.socialFooter').tooltip();
    </script>
  </span>
</div>

<!--
|
|   Google analytics
|
-->
<hr class='space'>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-29418712-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script');
    ga.type = 'text/javascript';
    ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(ga, s);
  })();

</script>