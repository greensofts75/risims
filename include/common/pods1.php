<hr class='space'>
<div class="span-8 smallSquare">
  <img src='images/310x100/products.jpg' alt='3'>
  <div class='arrowUp'></div>
  <div class='box'>
    <h6>Best Products and manufacturing plants</h6>
    <p>Nice artistic artifacts are available here.</p>
  </div>
</div>
<div class="span-8 smallSquare">
  <img src='images/310x100/nature.jpg' alt='Preserve Green'>
  <div class='arrowUp'></div>
  <div class='box'>
    <h6>Environment friendly products</h6>
    <p>We’re committed to preserve green. Join us!!</p>
  </div>
</div>
<div class="span-8 last smallSquare">
  <img src='images/310x100/clients.jpg' alt='Clients'>
  <div class='arrowUp'></div>
  <div class='box'>
    <h6>Happy clients all over the globe</h6>
    <p>Let our products touch your heart!</p>
  </div>
</div>
