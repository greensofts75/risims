<div class='span-24'>
  <script type="text/javascript">
    jQuery(function($) {
      $('ul#items').easyPaginate({
        step: 3,
        auto: false,
        clickstop: false,
        pause: 3000
      });
    });
  </script>
  <hr class='space'>
  <h1 class='white'>Product gallery</h1>
  <div class='carousel'>
    <ul id="items">
      <li class='first'>
        <p class="image"><a href="#"><img src="images/products/jute-product-small.jpg" alt="Jute products"></a></p>
        <div class='arrowUp'></div>
        <div class='box'>
          <h6>Jute products</h6>
          <p>Jute is one of the most valuable natural resources of Bangladesh and is truly ruled the world market.
            Bangladesh produces the finest quality natural jute products.
          </p>
        </div>
      </li>

      <li>
        <p class="image"><a href="#"><img src="images/products/erotion-control.jpg" alt="Template preview"></a></p>
        <div class='arrowUp'></div>
        <div class='box'>
          <h6>Erosion control and Geotextile products</h6>
          <p>An environmentally friendly way of protecting young trees and shrubs by reducing
            competing weeds and the need to use damaging herbicide. </p>
        </div>
      </li>

      <li>
        <p class="image"><a href="#"><img src="images/products/garments-products.jpg" alt="Garments products"></a></p>
        <div class='arrowUp'></div>
        <div class='box'>
          <h6>Garments and Textile products</h6>
          <p>We offer a wide variety of garments and textile products. Please contact us for more details.</p>
        </div>
      </li>

      <li class='first'>
        <p class="image"><a href="#"><img src="images/products/recycle-plastic-product.jpg" alt="Recycle  and Plastic products"></a></p>
        <div class='arrowUp'></div>
        <div class='box'>
          <h6>Recycle  and Plastic products</h6>
          <p>We offer a wide variety of recycle and plastic products. Please contact us for more details.</p>
        </div>
      </li>

      <li>
        <p class="image"><a href="#"><img src="images/products/export-import-products.jpg" alt="Export and Import products"></a></p>
        <div class='arrowUp'></div>
        <div class='box'>
          <h6>Export and Import products</h6>
          <p>We offer a wide variety of export and import products. Please contact us for more details.</p>
        </div>
      </li>

    </ul>
  </div>
</div>
