<div class='span-24 slider'>
  <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
      <li data-target="#carousel-example-generic" data-slide-to="1"></li>
      <li data-target="#carousel-example-generic" data-slide-to="2"></li>
    </ol>
    <!-- Wrapper for slides -->
    <div class="carousel-inner">
      <div class="item active">
        <img src="images/homepage-slider/reel-closeup-1.jpg" alt="...">
        <div class="carousel-caption">
          <h3></h3>
        </div>
      </div>
      <div class="item">
        <img src="images/homepage-slider/nature-friendly.jpg" alt="...">
        <div class="carousel-caption">
          <h3></h3>
        </div>
      </div>
      <div class="item">
        <img src="images/homepage-slider/fabric-closeup-1.jpg" alt="...">
        <div class="carousel-caption">
          <h3></h3>
        </div>
      </div>
    </div>
    <!-- Controls -->
    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
    </a>
    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
    </a>
  </div> <!-- Carousel -->
  <script>
    $('.carousel').carousel({
      interval: 3000
    })
  </script>



</div>
