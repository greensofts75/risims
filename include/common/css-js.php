

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap-theme.min.css">


<!-- Framework CSS -->
<link rel="stylesheet" href="css/screen.css" type="text/css" media="screen, projection">
<link rel="stylesheet" href="css/print.css" type="text/css" media="print">
<link rel="stylesheet" href="css/stylesheet.css" type="text/css" media="screen, projection">
<!--[if lt IE 8]><link rel="stylesheet" href="css/ie.css" type="text/css" media="screen, projection"><![endif]-->
<!--[if IE]>
<style>
.footer {background: url(images/ieFooterBg.png) repeat-x; color: #eee;}
span.twitterBubble {background: url(images/ieFooterBg.png) repeat-x;  color: #eee;}
.navi ul.menu li a.hover, .navi ul.menu li a:hover.under, .navi ul.menu li ul.sub li {background: url(images/whiteRGBA.png) repeat;}
.navi ul.menu li a {color: white;}
.navi ul.menu li a:hover, .navi ul.menu li ul.sub li a:hover {color: #333;}
.containter {text-align: left;}
body {background: url(images/bg.jpg) no-repeat 0% 0% fixed; text-align: center;
filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='images/bg.jpg', sizingMethod='scale');
-ms-filter: "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='images/bg.jpg', sizingMethod='scale')";}
img#bg {display: none;}
</style>
<![endif]-->
<!-- jQuery lib -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js" type="text/javascript"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
<!-- nivo slider -->
<script type="text/javascript" src="js/jquery.nivo.slider.js"></script>
<link rel="stylesheet" href="css/nivo.css" type="text/css" media="screen, projection">
<!-- jQuery carousel -->
<script type="text/javascript" src="js/easypaginate.js"></script>
<!-- custom tooltip -->
<script type="text/javascript" src="js/tooltip.js"></script>
<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300' rel='stylesheet' type='text/css'>

<link rel="stylesheet" href="css/bootstrap.vertical-tabs.css" type="text/css" media="screen, projection">


<style>
  body {color: #666; font-size: 90%; line-height: 1.2;}
  .container{padding: 0px;}
  ul.imagestack{
    margin: 0px;
    padding: 0px;
  }
  ul.imagestack li{
    list-style: none;
    width: 145px;
    height: 145px;
    overflow: hidden;
    margin: 10px 10px 0px 0px;
    float: left;
  }
  ul#items li {margin-left: 8px;}
</style>
  