<hr class='space'>
<div class="span-7">
  <div class='box noBg'>
    <a href='index.php'><img src='images/logo.png' alt='logo'></a>
  </div>
</div>
<div class="span-15 navi">
  <div class="" style="float: right; color: white">
    House # 127, Flat # A2, Road # 03, Block # A, Section # 12, Pallabi, Mirpur-1216, Dhaka,<br/>
Phone: 01670-188679 , 01919-155374</div>

  <ul class='menu'>
    <li><a href='index.php'>Home page</a></li>

    <li><a href='#'>Products</a>
      <ul class='sub'>
        <li><a href='jute-products.php'>Jute products</a></li>
        <li><a href='erotion-control-and-geotextile-products.php'>Erosion control and Geotextile products</a></li>        
        <li><a href='garments-and-textile-products.php'>Garments and Textile products</a></li>
        <li><a href='recycle-and-plastic-products.php'>Recycle and plastic products</a></li>
        <li class='last'><a href="export-and-import-products.php">Export and Import products</a></li>
      </ul>
    </li>
    <li><a href='contact.php' class='last'>Contact</a></li>
  </ul>
  <!-- Some jQuery to create slick animation -->
  <script type="text/javascript">
    (function() {
      var submenu = $('ul.sub'), // Set the class of the submenu
              linkHover = submenu.prev('a.under'); // Set the link to show submenu after hovering it

      submenu.on('mouseenter', function() {
        $(this).prev('a.under').addClass('hover');
      });
      submenu.on('mouseleave', function() {
        linkHover.removeClass('hover');
      });
      linkHover.on('mouseleave', function() {
        linkHover.removeClass('hover');
      });
    })();
  </script>
</div>