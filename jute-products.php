<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>RISIMS Group - Jute products</title>
    <meta charset="UTF-8">
    <meta name="description" content="Jute products">
    <meta name="keywords" content="jute product,Erosion control and Geotextile products,Garments and Textile products,Recycle  and Plastic products, Export and Import products.">
    <meta name="author" content="raihan sikder">
    <?php include './include/common/css-js.php'; ?>
  </head>
  <body>
    <img src='images/bg.jpg' id='bg' alt='background' style="display: block">
    <div class="container ">
      <?php include('./include/common/top.php'); ?>
      <?php include('./include/common/slider.php'); ?>
      <?php //include('./include/common/pods1.php'); ?>
      <hr class="space">
      <div class='span-24 maincontent'><!-- MAIN CONTENT STARTS HERE -->
        <hr class="space">
        <div class='box'>
          <h2 class='tl'>Jute Products</h2>
          <div class="span24">
            <div class="col-xs-3 "> <!-- required for floating -->
              <!-- Nav tabs -->
              <ul class="nav nav-tabs tabs-left">
                <li class="active"><a href="#coir" data-toggle="tab">Jute products</a></li>
               
              </ul>
            </div>

            <div class="col-xs-9">
              <!-- Tab panes -->
              <div class="tab-content">
                <div class="tab-pane active" id="coir"><?php include './include/products/jute-products/details.php'; ?></div>
                
              </div>
            </div>
          </div>
        </div>
        <hr class='space'>
        <!-- MIDDLE CONTENT STARTS HERE -->
      </div><!-- end of main content -->
      <?php include('./include/common/product-gallery.php'); ?>
      <?php include('./include/common/footer.php'); ?>
    </div><!-- end of containter -->
  </body>
</html>
