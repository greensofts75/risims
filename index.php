<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>RISIMS Group - Home</title>
    <meta charset="UTF-8">
    <meta name="description" content="We take pleasure to introduce ourselves as one of the leading jute goods manufacturers. We are an AUSTRALIA based company manufacturing unit in Bangladesh. ">
    <meta name="keywords" content="jute product,Erosion control and Geotextile products,Garments and Textile products,Recycle  and Plastic products, Export and Import products.">
    <meta name="author" content="raihan sikder">
    <?php include './include/common/css-js.php'; ?>
  </head>
  <body>
    <img src='images/bg.jpg' id='bg' alt='background' style="display: block">
    <div class="container">
      <?php include('./include/common/top.php'); ?>
      <?php include('./include/common/slider.php'); ?>
      <?php include('./include/common/pods1.php'); ?>
      <hr class="space">
      <div class='span-24 maincontent'><!-- MAIN CONTENT STARTS HERE -->
        <hr class="space">
        <div class="span-8">
          <div class='box'>
            <h2 class='tl'><img src='images/aboutus.png' alt='About Us'>About Us</h2>
            <p>We take pleasure to introduce ourselves as one of the leading jute goods manufacturers. We are an AUSTRALIA based company manufacturing unit in Bangladesh. </p>
            <!--            <a href='#' class='fr'>Read more...</a>-->
          </div>
        </div>
        <div class="span-8">
          <div class='box'>
            <h2 class='tl'><img src='images/whatwedo.png'  alt='Mission'>Mission</h2>
            <p>The mission of RISIMS GROUP is to provide better quality products and services to our clients. RISIMS GROUP always wants to see customers happy. Our customers are our asset.</p>
            <!--            <a href='#' class='fr'>Read more...</a>-->
          </div>
        </div>
        <div class="span-8 last">
          <div class='box'>
            <h2 class='tl'><img src='images/testimonies.png'  alt='Why Choose RISIMS Group'>Why RISIMS Group</h2>
            <p>We have strong presence in Bangladesh local market as well as worldwide. We follow international standard on product quality management.</p>
            <!--            <a href='#' class='fr'>Read more...</a>-->
          </div>
        </div>
        <hr class='space'>
        <!-- MIDDLE CONTENT STARTS HERE -->
      </div><!-- end of main content -->
      <?php include('./include/common/product-gallery.php'); ?>
      <?php include('./include/common/footer.php'); ?>
    </div><!-- end of containter -->
  </body>
</html>
