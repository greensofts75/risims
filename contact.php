<?php
session_start();
require_once("phpmailer/class.phpmailer.php");

//print_r($_SESSION);

$mailSentSuccess = FALSE;
$captchaCheck = FALSE;
if (isset($_POST['submit'])) {
  if ($_SESSION['captcha'] == trim($_REQUEST['captcha'])) {
    $name = $_POST['name'];
    $email = $_POST['email'];
    $phone = $_POST['phone'];
    $message = $_POST['message'];
    // Initiate PHPMailer
    unset($mail);
    $mail = new PHPMailer();
    $mail->From = "sales@risimsgroup.com";
    $mail->FromName = "RISIMS";
    $mail->AddReplyTo("sales@risimsgroup.com", "Sales");
    $mail->AddBCC("raihan.act@gmail.com", "Raihan");
    $mail->AddBCC("shahin12@gmail.com", "Shahin");
    $mail->AddBCC("tesshaz@gmail.com", "Shaz");

    $Body = "<table>"
            . "<tr>"
            . "<td>Name</td>"
            . "<td>$name</td>"
            . "</tr>"
            . "<tr>"
            . "<td>E-mail</td>"
            . "<td>$email</td>"
            . "</tr>"
            . "<tr>"
            . "<td>Phone</td>"
            . "<td>$phone</td>"
            . "</tr>"
            . "<tr>"
            . "<td>Message</td>"
            . "<td>$message</td>"
            . "</tr>"
            . "</table>";
    $mail->Body = $Body;
    $mail->AltBody = str_replace('<br/>', '\n\n', strip_tags($Body, '<br/>'));

    if (!$mail->Send()) {
      echo "Mailer Error: " . $mail->ErrorInfo;
    } else {
      $mailSentSuccess = TRUE;
    }
  } else {
    $captchaCheck = FALSE;
  }
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>RISIMS Group - Contact</title>
    <meta charset="UTF-8">
    <meta name="description" content="Contact us">
    <meta name="keywords" content="jute product,Erosion control and Geotextile products,Garments and Textile products,Recycle  and Plastic products, Export and Import products.">
    <meta name="author" content="raihan sikder">
    <?php include './include/common/css-js.php'; ?>
  </head>
  <body>
    <img src='images/bg.jpg' id='bg' alt='background'>
    <div class="container">
      <?php include('./include/common/top.php'); ?>
      <?php include('./include/common/slider.php'); ?>
      <?php //include('./include/common/pods1.php'); ?>
      <hr class='space'>
      <h1 class='white'>Contact</h1>
      <div class='span-24 maincontent'><!-- MAIN CONTENT STARTS HERE -->
        <!-- MIDDLE CONTENT STARTS HERE -->
        <div class='box'>
          <div class='span-12'>
            <?php if ($mailSentSuccess) { ?>
              <h2>Successfully submitted</h2>
              <p>Your request has been successfully placed</p>
            <?php } else { ?>
              <h2>Send us a message</h2>
              <?php
              if (isset($_POST['submit']) && !$captchaCheck) {
                echo "<div class='alert'>Image verification failed</div>";
              }
              ?>
              <form action='<?= $SERVER['PHP_SELF'] ?>' method='post'>
                <label>Your name</label>
                <input type='text' name='name' value="<?= $_REQUEST['name'] ?>"/>
                <label>Your email adress</label>
                <input type='text' name='email' value="<?= $_REQUEST['email'] ?>"/>
                <label>Your phone number</label>
                <input type='text' name='phone' value="<?= $_REQUEST['phone'] ?>"/>
                <label>Your message</label>

                <textarea cols='45' rows='7' name='message'><?= $_REQUEST['message'] ?></textarea>
                <div style="clear: both"></div>
                <label>Image verification</label>
                <input type='text' name='captcha' value="<?= $_REQUEST['captcha'] ?>"/>
                <img src="captcha/captcha.php"/>
                <input type='submit' name="submit" value='Send it'>
              </form>
            <?php } ?>
          </div>
        </div>
        <div class='box'>
          <div class='span-9'>
            <h2>You will find us here</h2>
            <div class='googleMap'><iframe width="370px" height="250" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.pl/maps?f=q&amp;source=s_q&amp;hl=pl&amp;geocode=&amp;q=New+York,+NY,+United+States&amp;aq=1&amp;oq=New+york&amp;sll=52.025459,19.204102&amp;sspn=12.492606,32.145996&amp;ie=UTF8&amp;hq=&amp;hnear=Nowy+Jork,+New+York,+Nowy+Jork,+Stany+Zjednoczone&amp;ll=40.714353,-74.005973&amp;spn=0.004952,0.009645&amp;t=m&amp;z=14&amp;iwloc=A&amp;output=embed"></iframe></div>
          </div>
          <hr class='space'>
          <div class='span-9 social'>
            <h2>Social Media</h2>
            <a href='#'><img src='images/social/big/facebook.png' alt='facebook'></a>
            <a href='#'><img src='images/social/big/twitter.png' alt='twitter'></a>
            <a href='#'><img src='images/social/big/tumblr.png' alt='tumblr'></a>
            <a href='#'><img src='images/social/big/skype.png' alt='skype'></a>
            <a href='#'><img src='images/social/big/vimeo.png' alt='vimeo'></a>
          </div>
        </div>
      </div><!-- end of main content -->
      <!-- twitter -->


      <?php include('./include/common/product-gallery.php'); ?>
      <?php include('./include/common/footer.php'); ?>
    </div><!-- end of containter -->
  </body>
</html>
