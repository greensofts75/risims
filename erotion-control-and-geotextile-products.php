<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>RISIMS Group - Erotion control and geotextile products</title>
    <meta charset="UTF-8">
    <meta name="description" content="Erotion control and geotextile products">
    <meta name="keywords" content="jute product,Erosion control and Geotextile products,Garments and Textile products,Recycle  and Plastic products, Export and Import products.">
    <meta name="author" content="raihan sikder">
    <?php include './include/common/css-js.php'; ?>
  </head>
  <body>
    <img src='images/bg.jpg' id='bg' alt='background' style="display: block">
    <div class="container ">
      <?php include('./include/common/top.php'); ?>
      <?php include('./include/common/slider.php'); ?>
      <?php //include('./include/common/pods1.php'); ?>
      <hr class="space">
      <div class='span-24 maincontent'><!-- MAIN CONTENT STARTS HERE -->
        <hr class="space">
        <div class='box'>
          <h2 class='tl'>Erotion control and geotextile products</h2>
          <div class="span24">
            <div class="col-xs-3"> <!-- required for floating -->
              <!-- Nav tabs -->
              <ul class="nav nav-tabs tabs-left">
                <li class="active"><a href="#coir" data-toggle="tab">Coir</a></li>
                <li><a href="#jute-mat" data-toggle="tab">Jute mat</a></li>
                <li><a href="#jute-nursery-product" data-toggle="tab">Jute nursery product</a></li>
                <li><a href="#soil-saver" data-toggle="tab">Soil saver</a></li>
              </ul>
            </div>

            <div class="col-xs-9">
              <!-- Tab panes -->
              <div class="tab-content">
                <div class="tab-pane active" id="coir"><?php include './include/products/erotion-control-and-geotextile-products/coir/details.php'; ?></div>
                <div class="tab-pane" id="jute-mat"><?php include './include/products/erotion-control-and-geotextile-products/jute_mat/details.php'; ?></div>
                <div class="tab-pane" id="jute-nursery-product"><?php include './include/products/erotion-control-and-geotextile-products/jute_nursery_product/details.php'; ?></div>
                <div class="tab-pane" id="soil-saver"><?php include './include/products/erotion-control-and-geotextile-products/soil_saver/details.php'; ?></div>
              </div>
            </div>
          </div>
        </div>
        <hr class='space'>
        <!-- MIDDLE CONTENT STARTS HERE -->
      </div><!-- end of main content -->
      <?php include('./include/common/product-gallery.php'); ?>
      <?php include('./include/common/footer.php'); ?>
    </div><!-- end of containter -->
  </body>
</html>
